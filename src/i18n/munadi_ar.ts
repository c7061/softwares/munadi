<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar_EG">
<context>
    <name>AdjustmentComponent</name>
    <message>
        <source>Volume</source>
        <translation>الصوت</translation>
    </message>
    <message>
        <source>%L1 %</source>
        <translation>٪%L1</translation>
    </message>
    <message>
        <source>+ %L1</source>
        <translation>%L1 +</translation>
    </message>
    <message>
        <source>- %L1</source>
        <translation>%L1 -</translation>
    </message>
    <message>
        <source>%1 %</source>
        <translation type="vanished">%1 ٪</translation>
    </message>
    <message>
        <source>+ %1</source>
        <translation type="vanished">+ %1</translation>
    </message>
    <message>
        <source>- %1</source>
        <translation type="vanished">- %1</translation>
    </message>
    <message>
        <source> %</source>
        <translation type="vanished"> ٪</translation>
    </message>
    <message>
        <source>+ </source>
        <translation type="vanished">+ </translation>
    </message>
    <message>
        <source>%L1</source>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>- </source>
        <translation type="vanished">- </translation>
    </message>
    <message>
        <source>%</source>
        <translation type="vanished">٪</translation>
    </message>
    <message>
        <source>Minutes</source>
        <translation>دقائق</translation>
    </message>
</context>
<context>
    <name>AudioManComp</name>
    <message>
        <source>Please choose an audio file</source>
        <translation>اختر ملفا صوتياً</translation>
    </message>
</context>
<context>
    <name>CompassPage</name>
    <message>
        <source>Qibla Direction</source>
        <translation>اتجاه القبلة</translation>
    </message>
    <message>
        <source>%1° with respect to North</source>
        <translation>%1° إلى الشمال</translation>
    </message>
</context>
<context>
    <name>DateRow</name>
    <message>
        <source>&lt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation>&lt;</translation>
    </message>
</context>
<context>
    <name>LocationInfoRow</name>
    <message>
        <source>Set your location ...</source>
        <translation>حدد مكانك ...</translation>
    </message>
</context>
<context>
    <name>LocationSearchComponent</name>
    <message>
        <source>Enter city name</source>
        <translation>أدخل اسم المدينة</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>بحث</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">الموقع</translation>
    </message>
    <message>
        <source>Timezone</source>
        <translation type="vanished">المنطقة الزمنية</translation>
    </message>
    <message>
        <source>Latitude</source>
        <translation type="vanished">خط عرض</translation>
    </message>
    <message>
        <source>Longitude</source>
        <translation type="vanished">خط طول</translation>
    </message>
</context>
<context>
    <name>MunadiModel</name>
    <message>
        <source>Makkah (built-in)</source>
        <translation>مكة (مدمج)</translation>
    </message>
    <message>
        <source>Muslim World League</source>
        <translation>رابطة العالم الإسلامي</translation>
    </message>
    <message>
        <source>Egyptian</source>
        <translation>مصري</translation>
    </message>
    <message>
        <source>Karachi</source>
        <translation>كراتشي</translation>
    </message>
    <message>
        <source>Umm AlQura</source>
        <translation>أم القرى</translation>
    </message>
    <message>
        <source>Dubai</source>
        <translation>دبي</translation>
    </message>
    <message>
        <source>Moonsighting Committee</source>
        <translation>لجنة الرؤية القمرية</translation>
    </message>
    <message>
        <source>North America</source>
        <translation>أمريكا الشمالية</translation>
    </message>
    <message>
        <source>Kuwait</source>
        <translation>الكويت</translation>
    </message>
    <message>
        <source>Qatar</source>
        <translation>قطر</translation>
    </message>
    <message>
        <source>Singapore</source>
        <translation>سنغافورة</translation>
    </message>
    <message>
        <source>Turkey</source>
        <translation>تركيا</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>اُخرى</translation>
    </message>
    <message>
        <source>Fajr</source>
        <translation>الفجر</translation>
    </message>
    <message>
        <source>Sunrise</source>
        <translation>الشروق</translation>
    </message>
    <message>
        <source>Dhuhr</source>
        <translation>الظهر</translation>
    </message>
    <message>
        <source>Asr</source>
        <translation>العصر</translation>
    </message>
    <message>
        <source>Maghrib</source>
        <translation>المغرب</translation>
    </message>
    <message>
        <source>Isha</source>
        <translation>العشاء</translation>
    </message>
    <message>
        <source>Hijri</source>
        <translation type="vanished">هجري</translation>
    </message>
    <message>
        <source>%1 Athan ...</source>
        <translation>أذان %1 ...</translation>
    </message>
    <message>
        <source>%L1</source>
        <translation>%L1</translation>
    </message>
    <message>
        <source>h </source>
        <translation>سـ </translation>
    </message>
    <message>
        <source>m </source>
        <translation>د </translation>
    </message>
    <message>
        <source>until </source>
        <translation>حتى </translation>
    </message>
    <message>
        <source>Seconds until </source>
        <translation>ثواني حتى </translation>
    </message>
    <message>
        <source>System</source>
        <translation>النظام</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>فاتح</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>داكن</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Application</source>
        <translation>تطبيق</translation>
    </message>
    <message>
        <source>Show on Athan</source>
        <translation>أظهر عند الأذان</translation>
    </message>
    <message>
        <source>Try to show window on Athan.</source>
        <translation>حاول إظهار النافذة عند الأذان.</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>تشغيل آلي</translation>
    </message>
    <message>
        <source>Try to start minimised after login.</source>
        <translation>حاول البدء مصغراً عند البداية.</translation>
    </message>
    <message>
        <source>Mute all Athans</source>
        <translation>أخفت أصوات الأذان</translation>
    </message>
    <message>
        <source>Athan</source>
        <translation>الأذان</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>الصلاة</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>اُخرى</translation>
    </message>
    <message>
        <source>Mathhab</source>
        <translation>المذهب</translation>
    </message>
    <message>
        <source>Majority</source>
        <translation>الجمهور</translation>
    </message>
    <message>
        <source>Hanafi</source>
        <translation>حنفي</translation>
    </message>
    <message>
        <source>Algorithm</source>
        <translation>الخوارزمية</translation>
    </message>
    <message>
        <source>+ %L1</source>
        <translation type="vanished">+ %L1</translation>
    </message>
    <message>
        <source>- %L1</source>
        <translation type="vanished">- %L1</translation>
    </message>
    <message>
        <source>Contact</source>
        <translation>ألتواصل</translation>
    </message>
    <message>
        <source>Support</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <source>Calendar</source>
        <translation type="vanished">التقويم</translation>
    </message>
    <message>
        <source>Hijri</source>
        <translation type="vanished">هجري</translation>
    </message>
    <message>
        <source>Gregorian</source>
        <translation type="vanished">غريغوري</translation>
    </message>
    <message>
        <source>+ </source>
        <translation type="vanished">+ %1</translation>
    </message>
    <message>
        <source>- </source>
        <translation type="vanished">- </translation>
    </message>
    <message>
        <source>About Munadi</source>
        <translation>ما هو منادي</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>الموقع</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>الإصدار</translation>
    </message>
    <message>
        <source>Hide on close</source>
        <translation>إختفي عند الاغلاق</translation>
    </message>
    <message>
        <source>When closing Munadi, hide it instead.</source>
        <translation>عند إغلاق منادي, إخفيه.</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>المظهر</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source> Athan ...</source>
        <translation type="vanished">أذان 1% ...</translation>
    </message>
    <message>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>QGuiApplication</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <source>%1 Athan ...</source>
        <translation type="vanished">أذان %1 ...</translation>
    </message>
    <message>
        <source>h </source>
        <translation type="vanished">س </translation>
    </message>
    <message>
        <source>%L1</source>
        <translation type="vanished">%L1</translation>
    </message>
    <message>
        <source>m until </source>
        <translation type="vanished">د حتى </translation>
    </message>
    <message>
        <source>Fajr</source>
        <translation type="vanished">الفجر</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">الرئيسية</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">الإعدادات</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>إنهاء</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>إظهار</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>إخفاء</translation>
    </message>
</context>
</TS>
