import QtQuick 2.0
import QtQuick.Controls.Material 2.0

Rectangle {
    color: Material.dividerColor
    height: 1
    opacity: 0.5
}
