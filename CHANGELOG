<h1>Munadi 18.12</h1>
<p>This is a minor release, some features were removed, and new ones added:</p>

<ul>
<li>Removed: Date can now go forward and backwards</li>
<li>Athan volumes can be configured individually</li>
<li>Added support for Android and Flatpack</li>
<li>UI enhancements</li>
</ul>

</br>

<h1>Munadi 17.02</h1>
<p>This release added requested features made by many of you,
some features were removed, and of course some UI enhancements:</p>

<ul>
<li>Brand new UI, using the latest and greatest from Qt</li>
<li>You can now toggle between Hijri date and Miladi</li>
<li>Mundadi will use system's proxy by default</li>
<li>Munadi is now almost three times lighter!</li>
<li>Date can now go forward and backwards</li>
<li>Removed Iqama functionality, sorry!</li>
<li>Added keyboard shortcuts, see help</li>
<li>Many issues fixed</li>
</ul>

</br>

<h1>Munadi 15.05</h1>
<p>There are many exciting changes in this release,
including changes to the user interface, functionality, speed and many more, see below:</p>

<ol>

<li>
<h3>Iqama Time (Experimental)</h3>
<ul>
<li>Get Iqama time from any Mosque</li>
<li>Add Mosques to your favourite</li>
<li>Add Mosques to Munadi's datase</li>
<li>Add monthly timetable to any Mosque</li>
<li>Edit Mosque's details</li>
<li>Edit Mosque's timetable</li>
<li>Note: Please be responsible, we can't validate every Mosque out there</li>
</ul>
</li>

</br>

<li>
<h3>User Interface</h3>
<ul>
<li>Added animation</li>
<li>Changed font and colour</li>
<li>Everything should be speedier</li>
</ul>
</li>

</br>

<li>
<h3>Other changes</h3>
<ul>
<li>Integrated help</li>
<li>Automatically detect DST</li>
<li>New calculation methods</li>
<li>You now search for your city instead of menu selection</li>
<li>Many other changes behind the scenes</li>
</ul>
</li>

</ol>

<p>Please visit Munadi.org and contact us for feedback</p>
<p>We now have a Twitter handle (@munadi_org) and a YouTube channel (see help section),
they'll be used for updates, news and help on using Munadi's new features.</p>
